package richtercloud.arquillian.primefaces.it;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import org.apache.commons.io.FileUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.formatter.Formatters;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omnifaces.utils.arquillian.ArquillianPrimeFaces;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author richter
 */
@RunWith(Arquillian.class)
@RunAsClient
public class ArquillianPrimefacesIT {
    private static final Logger LOGGER = LoggerFactory.getLogger(ArquillianPrimefacesIT.class);

    @Deployment(testable = false)
    @SuppressWarnings("SleepWhileInLoop")
    public static Archive<?> createDeployment0() throws TransformerException, XPathExpressionException, ParserConfigurationException, SAXException, IOException {
        WebArchive retValue = Maven.configureResolver().workOffline().resolve("richtercloud:arquillian-primefaces-it-web:war:1.0-SNAPSHOT")
                .withoutTransitivity().asSingle(WebArchive.class);
        ByteArrayOutputStream archiveContentOutputStream = new ByteArrayOutputStream();
        retValue.writeTo(archiveContentOutputStream, Formatters.VERBOSE);
        LOGGER.info(archiveContentOutputStream.toString());
        return retValue;
    }

    @Drone
    private WebDriver browser;
    @ArquillianResource
    private URL deploymentUrl;
    @FindBy(id = "mainForm:autoComplete")
    private WebElement autoComplete;
    private final File screenshotDir;
    private int screenshotCounter;

    public ArquillianPrimefacesIT() throws IOException {
        this.screenshotDir = Files.createTempDirectory(ArquillianPrimefacesIT.class.getSimpleName()).toFile();
    }

    @Test
    public void testAll() throws IOException {
        String url = generateURL("index.xhtml");
        LOGGER.info(String.format("url: %s",
                url));
        browser.get(url);
        screenshot();
        new WebDriverWait(browser, 2).until(ExpectedConditions.visibilityOf(autoComplete));
        ArquillianPrimeFaces.setAutoCompleteValue(autoComplete,
                "item0", //query
                "item0" //value
        );
    }

    private String generateURL(String page) {
        String retValue = String.format("%s%s",
                deploymentUrl.toExternalForm(),
                page);
        return retValue;
    }

    private void screenshot() throws IOException {
        File scrFile = ((TakesScreenshot)browser).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(screenshotDir,
                String.format("%05d.png", screenshotCounter++)));
    }
}
