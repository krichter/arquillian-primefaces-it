package richtercloud.arquillian.primefaces.it.web;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author richter
 */
@Named
@ViewScoped
public class BackingBean0 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String value;
    private List<String> values = new LinkedList<>(Arrays.asList("item0", "item1", "item2"));

    public BackingBean0() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<String> completeSearchText(String input) {
        return values.stream().filter(item -> item.startsWith(input))
                .collect(Collectors.toList());
    }
}
